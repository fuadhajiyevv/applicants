package az.applicants.service.impl.security;

import az.applicants.dao.entity.TokenEntity;
import az.applicants.dao.entity.UserCredentialsEntity;
import az.applicants.dao.entity.UserInfoEntity;
import az.applicants.dao.repository.TokenRepository;
import az.applicants.dao.repository.UserCredentialsRepository;
import az.applicants.dto.request.security.AuthRequest;
import az.applicants.dto.request.security.RefreshTokenRequest;
import az.applicants.dto.request.security.RegisterRequest;
import az.applicants.dto.request.security.ResetPasswordRequest;
import az.applicants.dto.response.security.*;
import az.applicants.exceptions.OtpIsNotValidException;
import az.applicants.exceptions.TokenIsExpired;
import az.applicants.exceptions.TokenIsNotRefresh;
import az.applicants.exceptions.UniqueConstraintException;
import az.applicants.mapper.UserBuilder;
import az.applicants.service.AuthenticationService;
import az.applicants.validation.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Logger;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {


    private final Logger logger = LoggerFactory.getLogger();

    private final UserDetailsService userDetailsService;
    private final BCryptPasswordEncoder passwordEncoder;

    private final TokenRepository tokenRepository;

    private final JwtService jwtService;

    private final AuthenticationManager authenticationManager;

    private final UserCredentialsRepository userCredentialsRepository;

    private final RedisTemplate<String, Long> redisTemplate;

    private final JavaMailSender javaMailSender;

    public AuthenticationServiceImpl(UserDetailsService userDetailsService, BCryptPasswordEncoder passwordEncoder, TokenRepository tokenRepository, JwtService jwtService, AuthenticationManager authenticationManager, UserCredentialsRepository userCredentialsRepository, RedisTemplate<String, Long> redisTemplate, JavaMailSender javaMailSender) {
        this.userDetailsService = userDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.tokenRepository = tokenRepository;
        this.jwtService = jwtService;
        this.authenticationManager = authenticationManager;
        this.userCredentialsRepository = userCredentialsRepository;
        this.redisTemplate = redisTemplate;
        this.javaMailSender = javaMailSender;
    }


    @Override
    public AuthResponse authenticate(AuthRequest request) throws AuthenticationException {

        if (request == null) throw new AuthenticationServiceException("authentication object not found");

        Authentication authentication = new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword());
        try {
            authenticationManager.authenticate(authentication);
        } catch (AuthenticationException ex) {
            logger.warning("Authentication doesnt pass");
        }

        UserCredentialsEntity userDetails = (UserCredentialsEntity)
                userDetailsService.loadUserByUsername(authentication.getPrincipal().toString());


        revokeAllTokens(userDetails);

        String accessToken = jwtService.generateAccessToken(userDetails);
        String refreshToken = jwtService.generateRefreshToken(userDetails);

        tokenRepository.save(saveNewToken(accessToken, userDetails));

        return AuthResponse.builder()
                .isAuthenticated(true)
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
    }


    public RegisterResponse register(RegisterRequest request) throws AuthenticationException {

        if (request == null) {
            throw new AuthenticationServiceException("auth.is.null");
        }

        if (userCredentialsRepository.existsByEmailAndUserInfoEntityPhoneNumber(request.getEmail(), request.getPhoneNumber())) {
            throw new UniqueConstraintException("unique.violate");
        }

        String passwordToHash = passwordEncoder.encode(request.getPassword());

        UserInfoEntity userInfoEntity = UserBuilder.toUserInfoEntity(request.getName(), request.getSurname(), request.getPhoneNumber());
        UserCredentialsEntity userCredentialsEntity = UserBuilder.toUserCredentialsEntity(request.getEmail(), passwordToHash);

        userInfoEntity.setUserCredentialsEntity(userCredentialsEntity);
        userCredentialsEntity.setUserInfoEntity(userInfoEntity);

        userCredentialsRepository.save(userCredentialsEntity);

        return RegisterResponse
                .builder()
                .isRegistered(true)
                .build();
    }

    private void revokeAllTokens(UserCredentialsEntity entity) {
        TokenEntity token = tokenRepository.findValidToken(entity.getId()).orElse(null);
        if (Objects.isNull(token)) {
            return;
        }
        token.setRevoked(true);
        token.setExpired(true);
        tokenRepository.save(token);
    }

    private TokenEntity saveNewToken(String token, UserCredentialsEntity userDetails) {
        return TokenEntity.builder()
                .revoked(false)
                .expired(false)
                .token(token)
                .userCredentialsEntity(userDetails)
                .createdAt(new Date())
                .build();
    }


    public RefreshTokenResponse refreshToken(RefreshTokenRequest request) {

        String email = jwtService.extractSubject(request.getToken());
        UserCredentialsEntity userDetails = (UserCredentialsEntity) userDetailsService.loadUserByUsername(email);

        if (tokenRepository.findByToken(request.getToken()).isPresent()) {
            throw new TokenIsNotRefresh("token.is.not.refresh");
        }

        if (!jwtService.isNotExpired(request.getToken())) {
            throw new TokenIsExpired("refresh.token.expired");
        }

        String accessToken = jwtService.generateAccessToken(userDetails);
        String refreshToken = jwtService.generateRefreshToken(userDetails);

        revokeAllTokens(userDetails);
        tokenRepository.save(saveNewToken(accessToken, userDetails));


        return RefreshTokenResponse.builder()
                .accessToken(accessToken)
                .refreshToken(refreshToken)
                .build();
    }


    @Override
    public ResetPasswordResponse usernameValidation(String username) {
        UserCredentialsEntity userCredentials = (UserCredentialsEntity) userDetailsService.loadUserByUsername(username);
        SecureRandom secureRandom = null;
        try {
            secureRandom = SecureRandom.getInstance("SHA1PRNG");
        } catch (NoSuchAlgorithmException exception) {
            exception.getLocalizedMessage();
        }
        assert secureRandom != null;
        int getOtp = Math.abs(secureRandom.nextInt());
        redisTemplate.opsForValue().set(username, (long) getOtp);

        final String textPrefix = String.format("Your OTP is: %d",getOtp);
        sendMail(userCredentials.getEmail(), textPrefix);

        redisTemplate.expire(username, Duration.ofMinutes(2));

        return ResetPasswordResponse.builder()
                .totpIsGenerated(true)
                .usernameIsFound(true)
                .build();
    }

    private void sendMail(String to, String text) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setFrom("fuad.hajiyev.2003@mail.ru");
        simpleMailMessage.setTo(to);
        simpleMailMessage.setSubject("Password Reset");
        simpleMailMessage.setText(text);
        simpleMailMessage.setSentDate(new Date());
        javaMailSender.send(simpleMailMessage);
    }

    @Override
    public UpdatePasswordResponse resetPassword(ResetPasswordRequest request) {
        final String postFix = "_validation";
        long result = Objects.requireNonNull(redisTemplate.opsForValue().get(request.getUsername() + postFix));
        UserCredentialsEntity userCredentials = (UserCredentialsEntity) userDetailsService.loadUserByUsername(request.getUsername());
        if (result != 0) {
            throw new OtpIsNotValidException("otp_is_not_valid");
        }
        String updatedPassword = passwordEncoder.encode(request.getRepeatedNewPassword());
        userCredentials.setHash(updatedPassword);
        revokeAllTokens(userCredentials);
        return UpdatePasswordResponse.builder()
                .isUpdated(true)
                .build();
    }

    @Override
    public OTPCheckResponse enterNumber(String username, Long totp) {

        Long result = redisTemplate.getExpire(username);

        if (Objects.nonNull(result) && result != -2) {
            long value = Objects.requireNonNull(redisTemplate.opsForValue().get(username));
            if (value == totp) {
                final String postFix = "_validation";
                redisTemplate.delete(username);
                redisTemplate.opsForValue().set(username + postFix, 0L);
                return OTPCheckResponse
                        .builder()
                        .isValid(true)
                        .build();
            }
        }
        return OTPCheckResponse.builder()
                .isValid(false)
                .build();
    }


}
