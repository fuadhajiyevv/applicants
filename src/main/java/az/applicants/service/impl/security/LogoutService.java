package az.applicants.service.impl.security;

import az.applicants.dao.entity.TokenEntity;
import az.applicants.dao.repository.TokenRepository;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

@Component
public class LogoutService implements LogoutHandler {

    private final TokenRepository tokenRepository;
    private final JwtService jwtService;


    public LogoutService(TokenRepository tokenRepository, JwtService jwtService) {
        this.tokenRepository = tokenRepository;
        this.jwtService = jwtService;
    }

    @Override
    public void logout(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull Authentication authentication
    ) {
        String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);

        if (authHeader == null || !authHeader.startsWith("Bearer")) {
            return;
        }

        String token = authHeader.substring(7);
        String extractedUsernameFromToken = jwtService.extractSubject(token);
        TokenEntity validToken = tokenRepository.findByUserCredentialsEntityUsername(extractedUsernameFromToken);

        validToken.setExpired(true);
        validToken.setRevoked(true);

        tokenRepository.save(validToken);
        SecurityContextHolder.clearContext();
    }
}
