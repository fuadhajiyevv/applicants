package az.applicants.service.impl.security;

import az.applicants.dao.entity.UserCredentialsEntity;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Utf8;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.util.Date;
import java.util.function.Function;

@Service
public class JwtService {

    @Value("${jwt.access-token-duration}")
    private long accessTokenDuration;

    @Value("${jwt.refresh-token-duration}")
    private long refreshTokenDuration;

    @Value("${jwt.key}")
    private String key;

    @Value("${jwt.issuer}")
    private String issuer;


    private String generateJws(UserCredentialsEntity userDetails, Long duration) {
        long currentTimeMillis = System.currentTimeMillis();

        return Jwts.builder()
                .setHeaderParam("alg", "HS256")
                .setHeaderParam("typ", "JWT")
                .setIssuer(issuer)
                .setSubject(userDetails.getUsername())
                .claim("role", userDetails.getAuthorities())
                .setIssuedAt(new Date(currentTimeMillis))
                .setExpiration(new Date(currentTimeMillis + duration))
                .signWith(getKey())
                .compact();
    }

    private Key getKey() {
        byte[] keyArr = Utf8.encode(key);
        return Keys.hmacShaKeyFor(keyArr);
    }


    public String generateAccessToken(UserCredentialsEntity userDetails) {
        return generateJws(userDetails, accessTokenDuration);
    }

    public String generateRefreshToken(UserCredentialsEntity userDetails) {
        return generateJws(userDetails, refreshTokenDuration);
    }


    private Claims extractAllClaims(String token) {
        Jws<Claims> claimsJws = Jwts.parserBuilder()
                .setSigningKey(getKey())
                .build()
                .parseClaimsJws(token);

        return claimsJws.getBody();
    }


    private <T> T extractClaim(String token, Function<Claims, T> function) {
        Claims allClaims = extractAllClaims(token);
        return function.apply(allClaims);
    }

    public boolean isNotExpired(String token) {
        Date expirationDate = extractClaim(token, Claims::getExpiration);
        return new Date().before(expirationDate);
    }

    public String extractSubject(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public boolean isValid(UserDetails userDetails, String token) {
        String tokenSubject = extractSubject(token);
        return userDetails.getUsername().equals(tokenSubject) && isNotExpired(token);
    }


}
