package az.applicants.service;

import az.applicants.dto.request.security.AuthRequest;
import az.applicants.dto.request.security.RefreshTokenRequest;
import az.applicants.dto.request.security.RegisterRequest;
import az.applicants.dto.request.security.ResetPasswordRequest;
import az.applicants.dto.response.security.*;

import java.security.NoSuchAlgorithmException;

public interface AuthenticationService {

    AuthResponse authenticate(AuthRequest request);

    RegisterResponse register(RegisterRequest request);

    RefreshTokenResponse refreshToken(RefreshTokenRequest refreshTokenRequest);

    UpdatePasswordResponse resetPassword(ResetPasswordRequest request);

    ResetPasswordResponse usernameValidation(String username) throws NoSuchAlgorithmException;

    OTPCheckResponse enterNumber(String username,Long totp);


}
