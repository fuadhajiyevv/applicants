package az.applicants.dto.request.security;

import az.applicants.validation.annotations.Email;
import az.applicants.validation.annotations.PhoneNumber;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RegisterRequest {

    @Size(min = 2)
    @NotEmpty
    @NotBlank
    private String name;

    @Size(min = 2)
    @NotEmpty
    @NotBlank
    private String surname;

    @Email
    @NotEmpty
    @NotBlank
    private String email;

    @Size(min = 8)
    @NotEmpty
    @NotBlank
    private String password;

    @JsonProperty("phone_number")
    @PhoneNumber
    @NotEmpty
    @NotBlank
    private String phoneNumber;

}
