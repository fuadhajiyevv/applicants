package az.applicants.dto.request.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResetPasswordRequest {

    @JsonProperty(value = "username")
    private String username;

    @JsonProperty(value = "new_password")
    private String newPassword;

    @JsonProperty(value = "repeat_new_password")
    private String repeatedNewPassword;

}
