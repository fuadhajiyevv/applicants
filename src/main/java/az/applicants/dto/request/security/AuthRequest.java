package az.applicants.dto.request.security;

import az.applicants.validation.annotations.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
public class AuthRequest {

    @Email
    @NotEmpty
    @NotBlank
    private String email;

    @Size(min = 8)
    @NotEmpty
    @NotBlank
    private String password;
}
