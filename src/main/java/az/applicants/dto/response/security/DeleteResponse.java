package az.applicants.dto.response.security;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DeleteResponse {

    @JsonProperty(value = "is_deleted")
    private boolean isDeleted;
}
