package az.applicants.dto.response.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class UpdatePasswordResponse {
    @JsonProperty(value = "is_updated")
    private boolean isUpdated;

}
