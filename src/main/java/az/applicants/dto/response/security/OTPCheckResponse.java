package az.applicants.dto.response.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OTPCheckResponse {
    @JsonProperty(value = "is_valid")
    boolean isValid;
}
