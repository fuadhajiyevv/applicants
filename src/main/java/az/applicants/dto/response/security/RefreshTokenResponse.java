package az.applicants.dto.response.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class RefreshTokenResponse {

    @JsonProperty(value = "updated_access_token")
    private String accessToken;

    @JsonProperty(value = "updated_refresh_token")
    private String refreshToken;
}
