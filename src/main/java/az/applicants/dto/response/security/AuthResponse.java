package az.applicants.dto.response.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.springframework.beans.factory.annotation.Value;

@Builder
public class AuthResponse {

    @JsonProperty(value = "is_authenticated")
    @Value(value = "true")
    private boolean isAuthenticated;

    @JsonProperty(value = "access_token")
    private String accessToken;

    @JsonProperty(value = "refresh_token")
    private String refreshToken;
}
