package az.applicants.dto.response.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class RegisterResponse {
    @JsonProperty(value = "is_registered")
    private boolean isRegistered;
}
