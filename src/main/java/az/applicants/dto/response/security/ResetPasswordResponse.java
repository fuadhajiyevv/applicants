package az.applicants.dto.response.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResetPasswordResponse {
    @JsonProperty(value = "username_is_found")
    boolean usernameIsFound;
    @JsonProperty(value = "totp_is_generated")
    boolean totpIsGenerated;
}
