package az.applicants.enums;

import lombok.Getter;

@Getter
public enum Role {

    USER("USER"),
    ADMIN("ADMIN"),
    PREMIUM("PREMIUM");


    private final String name;

    Role(String name) {
        this.name = name;
    }
}
