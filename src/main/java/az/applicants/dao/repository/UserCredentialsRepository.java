package az.applicants.dao.repository;

import az.applicants.dao.entity.UserCredentialsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserCredentialsRepository extends JpaRepository<UserCredentialsEntity, Long> {

    Optional<UserCredentialsEntity> findByEmail(String email);


    boolean existsByEmailAndUserInfoEntityPhoneNumber(String email,String phoneNumber);

}
