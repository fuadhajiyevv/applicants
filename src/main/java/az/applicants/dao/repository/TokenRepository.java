package az.applicants.dao.repository;

import az.applicants.dao.entity.TokenEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TokenRepository extends JpaRepository<TokenEntity,Long> {


    Optional<TokenEntity> findByToken(String jwt);

    @Query(value = "select t.* from tokens t where user_credentials_id = :id and expired = false",nativeQuery = true)
    Optional<TokenEntity> findValidToken(Long id);

    @Query(value = "select t.* from tokens t join user_credentials u on t.user_credentials_id = u.id where u.email = :email",nativeQuery = true)
    TokenEntity findByUserCredentialsEntityUsername(String email);
}
