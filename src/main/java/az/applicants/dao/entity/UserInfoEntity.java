package az.applicants.dao.entity;

import az.applicants.validation.annotations.PhoneNumber;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.Objects;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "user_info",uniqueConstraints = @UniqueConstraint(columnNames = {"phone_number"}))
public class UserInfoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "phone_number", unique = true)
    @PhoneNumber
    private String phoneNumber;

    @JoinColumn(name = "user_credentials_id", referencedColumnName = "id")
    @OneToOne(targetEntity = UserCredentialsEntity.class, cascade = CascadeType.ALL)
    private UserCredentialsEntity userCredentialsEntity;

    @Column(name = "created_at")
    private Date createdAt;

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, phoneNumber, userCredentialsEntity);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || obj.getClass() != this.getClass()) return false;

        UserInfoEntity entity = (UserInfoEntity) obj;

        return Objects.equals(id, entity.getId()) &&
                Objects.equals(name, entity.getName()) &&
                Objects.equals(surname, entity.surname) &&
                Objects.equals(phoneNumber, entity.getPhoneNumber()) &&
                Objects.equals(getUserCredentialsEntity(), entity.userCredentialsEntity);
    }
}
