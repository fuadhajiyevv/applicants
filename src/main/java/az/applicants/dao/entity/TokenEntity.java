package az.applicants.dao.entity;

import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "tokens")
public class TokenEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "token",nullable = false)
    private String token;

    @Column(name = "expired")
    private Boolean expired;

    @Column(name = "revoked")
    private Boolean revoked;

    @Column(name = "created_at")
    private Date createdAt;

    @ManyToOne(targetEntity = UserCredentialsEntity.class)
    @JoinColumn(name = "user_credentials_id", referencedColumnName = "id")
    private UserCredentialsEntity userCredentialsEntity;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenEntity that = (TokenEntity) o;
        return Objects.equals(id, that.id) && Objects.equals(token, that.token) && Objects.equals(expired, that.expired) && Objects.equals(revoked, that.revoked) && Objects.equals(userCredentialsEntity, that.userCredentialsEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, token, expired, revoked, userCredentialsEntity);
    }

    @Override
    public String toString() {
        return "TokenEntity{" +
                "id=" + id +
                ", token='" + token + '\'' +
                ", expired=" + expired +
                ", revoked=" + revoked +
                ", userCredentials=" + userCredentialsEntity +
                '}';
    }
}
