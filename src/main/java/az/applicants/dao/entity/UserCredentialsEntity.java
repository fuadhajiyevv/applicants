package az.applicants.dao.entity;

import az.applicants.enums.Role;
import az.applicants.validation.annotations.Email;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Objects;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "user_credentials", uniqueConstraints = @UniqueConstraint(columnNames = {"email"}))
public class UserCredentialsEntity implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @Email
    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "hash", nullable = false)
    private String hash;

    @Column(name = "hash_function", columnDefinition = "VARCHAR(10) default 'bcrypt'")
    private String hashFunction;

    @Enumerated(value = EnumType.STRING)
    @Column(name = "role", columnDefinition = "VARCHAR(10) default 'USER'")
    private Role role;

    @Column(name = "created_at")
    private Date createdAt;

    @OneToOne(targetEntity = UserInfoEntity.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_info_id")
    private UserInfoEntity userInfoEntity;


    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;

        if (obj == null || obj.getClass() != this.getClass()) return false;

        UserCredentialsEntity entity = (UserCredentialsEntity) obj;

        return Objects.equals(id, entity.getId()) &&
                Objects.equals(email, entity.getEmail()) &&
                Objects.equals(hash, entity.getHash()) &&
                Objects.equals(hashFunction, entity.getHashFunction()) &&
                Objects.equals(createdAt, entity.getCreatedAt()) &&
                Objects.equals(role, entity.getRole()) &&
                Objects.equals(userInfoEntity, entity.getUserInfoEntity());

    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, hash, hashFunction, userInfoEntity, createdAt);
    }


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("ROLE_".concat(role.getName())));
    }

    @Override
    public String getPassword() {
        return hash;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
