package az.applicants.validation;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class LoggerFactory {

    private static final String LOGGER_NAME = "MyCustomLoggerName";

    @Getter
    private static final Logger logger = Logger.getLogger(LOGGER_NAME);

}
