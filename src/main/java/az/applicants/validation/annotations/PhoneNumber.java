package az.applicants.validation.annotations;

import az.applicants.validation.PhoneNumberValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = PhoneNumberValidator.class)
@Documented
public @interface PhoneNumber {

    String message() default "validation.phoneNumber.default_phone_number";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
