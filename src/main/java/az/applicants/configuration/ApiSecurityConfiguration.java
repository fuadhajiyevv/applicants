package az.applicants.configuration;

import az.applicants.dao.repository.TokenRepository;
import az.applicants.enums.Role;
import az.applicants.service.impl.security.JwtFilter;
import az.applicants.service.impl.security.JwtService;
import az.applicants.service.impl.security.LogoutService;
import az.applicants.service.impl.security.UserDetailsServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.logout.LogoutFilter;

@Configuration
public class ApiSecurityConfiguration {

    private final AuthenticationManager authenticationManager;

    private final LogoutService logoutService;

    private final JwtService jwtService;

    private final TokenRepository tokenRepository;

    private final UserDetailsServiceImpl userDetailsService;
    private final DaoAuthenticationProvider authenticationProvider;

    public ApiSecurityConfiguration(AuthenticationManager providerManager, LogoutService logoutService, JwtService jwtService, TokenRepository tokenRepository, UserDetailsServiceImpl userDetailsService, DaoAuthenticationProvider authenticationProvider) {
        this.authenticationManager = providerManager;
        this.logoutService = logoutService;
        this.jwtService = jwtService;
        this.tokenRepository = tokenRepository;
        this.userDetailsService = userDetailsService;
        this.authenticationProvider = authenticationProvider;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity web) throws Exception {
        return web
                .csrf(CsrfConfigurer::disable)
                .authenticationManager(authenticationManager)
                .authenticationProvider(authenticationProvider)
                .authorizeHttpRequests(
                        requestMatcherRegistry ->
                                requestMatcherRegistry
                                        .requestMatchers("/auth/**").permitAll()
                                        .requestMatchers("/admin/**").hasRole(Role.ADMIN.getName())
                                        .anyRequest().authenticated()
                )
                .logout(
                        http -> {
                            http.addLogoutHandler(logoutService);
                            http.logoutUrl("/logout");
                            http.clearAuthentication(true);
                            http.logoutSuccessHandler(
                                    (request, response, authentication) -> SecurityContextHolder.clearContext()
                            );
                            http.permitAll();
                        }
                )
                .sessionManagement(
                        sessionManagementConfigurer -> {
                            sessionManagementConfigurer.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
                        }
                )
                .addFilterAfter(new JwtFilter(jwtService, userDetailsService, tokenRepository), LogoutFilter.class)
                .build();
    }
}
