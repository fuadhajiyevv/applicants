package az.applicants.configuration;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Component
public class LogFactory {

    private static final String LOGGER_NAME = "MyCustomLoggerName";

    @Getter
    private static final Logger logger = Logger.getLogger(LOGGER_NAME);

}
