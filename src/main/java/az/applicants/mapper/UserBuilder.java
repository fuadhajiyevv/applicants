package az.applicants.mapper;

import az.applicants.dao.entity.UserCredentialsEntity;
import az.applicants.dao.entity.UserInfoEntity;
import az.applicants.enums.Role;
import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public class UserBuilder {

    @Value("${spring.security.hash.function}")
    private static String hashFunction;


    public static UserInfoEntity toUserInfoEntity(
            String name,
            String surname,
            String phoneNumber
    ) {
        return UserInfoEntity.builder()
                .name(name)
                .surname(surname)
                .phoneNumber(phoneNumber)
                .createdAt(new Date())
                .build();
    }

    public static UserCredentialsEntity toUserCredentialsEntity(
            String email,
            String password
    ) {
        return UserCredentialsEntity.builder()
                .email(email)
                .hash(password)
                .createdAt(new Date())
                .hash(password)
                .hashFunction(hashFunction)
                .role(Role.USER)
                .build();
    }
}
