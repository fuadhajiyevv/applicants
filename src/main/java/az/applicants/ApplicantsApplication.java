package az.applicants;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.NoSuchAlgorithmException;

@SpringBootApplication
public class ApplicantsApplication {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        SpringApplication.run(ApplicantsApplication.class);
    }

}
