package az.applicants.controller.auth;

import az.applicants.dto.request.security.AuthRequest;
import az.applicants.dto.request.security.RefreshTokenRequest;
import az.applicants.dto.request.security.RegisterRequest;
import az.applicants.dto.request.security.ResetPasswordRequest;
import az.applicants.dto.response.security.*;
import az.applicants.service.impl.security.AuthenticationServiceImpl;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;

@RestController
@RequestMapping("/auth")
public class AuthController {
    private final AuthenticationServiceImpl authenticationService;

    public AuthController(AuthenticationServiceImpl authenticationService) {
        this.authenticationService = authenticationService;
    }


    @PostMapping("/login")
    public AuthResponse login(
            @RequestBody AuthRequest request
    ) {
        return authenticationService.authenticate(request);
    }


    @PostMapping("/register")
    public RegisterResponse register(
            @RequestBody RegisterRequest request
    ) {
        return authenticationService.register(request);
    }

    @PostMapping("/refresh")
    public RefreshTokenResponse refresh(
            @RequestBody RefreshTokenRequest request
    ) {
        return authenticationService.refreshToken(request);
    }

    @PostMapping("/init/validation/{username}")
    public ResetPasswordResponse reset(
            @PathVariable(name = "username") String username
    ) throws NoSuchAlgorithmException {
        return authenticationService.usernameValidation(username);
    }

    @PostMapping("/enter")
    public OTPCheckResponse enterOtp(
            @RequestParam(name = "username") String username,
            @RequestParam(name = "otp") Long otp) {

        return authenticationService.enterNumber(username, otp);
    }

    @PostMapping("/reset")
    public UpdatePasswordResponse resetPassword(
            @RequestBody ResetPasswordRequest request
    ) {
        return authenticationService.resetPassword(request);
    }

}
